```appinio_social_share``` supports sharing files to social media (Facebook, Instagram, Instagram Story, Messenger, Telegram, WhatsApp, Twitter, Tiktok, SMS, System, etc.). If you want to share text, file, image, text with image or text with files then this plugin is all you need.

<br />


## This lib is the fork from lib https://github.com/appinioGmbH/flutter_packages/tree/main/packages/appinio_social_share

## There are some changes in this repo
# Flutter
* sdk: ">=2.16.2 <3.0.0"

# Android
* We use fileprovider instead of provider 

